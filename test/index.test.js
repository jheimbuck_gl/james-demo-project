const expect = require('chai').expect;
const sum = require('../index');
const dif = require('../index');

describe('index.js', function () {
  it('adds 1 + 2 to equal 3', function () {
    expect(sum(1, 2)).to.equal(3);
  });

});

describe('index.js', function () {
    it('subtract 3 - 2 to equal 1', function () {
        expect(dif(3,2)).to.equal(1);
    });
    
});